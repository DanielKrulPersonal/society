<?php
class Response {
	public $errorMessage;
	public $statusCode;

	public function __toString()
	{
		return json_encode($this);
	}
}