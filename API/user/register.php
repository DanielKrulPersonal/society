<?php
require_once '../init.php';

class RegisterCodesAndMessages {
	const ERROR_EMAIL_EXISTS_CODE = 20;
	const ERROR_EMAIL_EXISTS_MESSAGE = 'email-exists';

	const ERROR_USERNAME_EXISTS_CODE = 40;
	const ERROR_USERNAME_EXISTS_MESSAGE = 'username-exists';

	const SUCCESS_REGISTERED_CODE = 60;
}

class RegisterResponse extends Response {
	const EMAIL_PARAM = 'email';
	const PASSWORD_PARAM = 'password';
	const USERNAME_PARAM = 'username';

	private $email;
	private $username;
	private $password;
	private $passwordHash;

	public function __construct ($email, $password, $username) {
		$this->email = Utils::htmlescape(trim($email));
		$this->password = $password;
		$this->username = Utils::htmlescape(trim($username));
		$this->passwordHash = hash('sha256', $password);

		if($this->ifEmailExists()) {
			$this->errorMessage = RegisterCodesAndMessages::ERROR_EMAIL_EXISTS_MESSAGE;
			$this->statusCode = RegisterCodesAndMessages::ERROR_EMAIL_EXISTS_CODE;

			return;
		}
		if($this->ifUsernameExists()) {
			$this->errorMessage = RegisterCodesAndMessages::ERROR_USERNAME_EXISTS_MESSAGE;
			$this->statusCode = RegisterCodesAndMessages::ERROR_USERNAME_EXISTS_CODE;

			return;
		}

		$this->registerUser();
	}

	private function registerUser() {
		Database::$db->query('INSERT INTO users', [
			'email' => $this->email,
			'password' => $this->passwordHash,
			'username' => $this->username,
			'token' => Utils::getUniqueMd5Token()
		]);

		$this->statusCode = RegisterCodesAndMessages::SUCCESS_REGISTERED_CODE;
	}

	private function ifUsernameExists() {
		$data = Database::$db->fetch("SELECT 1 FROM users WHERE username = %s", $this->username);
		return (boolean)$data;
	}

	private function ifEmailExists() {
		$data = Database::$db->fetch("SELECT 1 FROM users WHERE email = %s", $this->email);
		return (boolean)$data;
	}
}

echo new RegisterResponse(
	Utils::request(RegisterResponse::EMAIL_PARAM),
	Utils::request(RegisterResponse::PASSWORD_PARAM),
	Utils::request(RegisterResponse::USERNAME_PARAM)
);
