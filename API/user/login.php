<?php
require_once '../init.php';

class LoginCodesAndMessages {
	const ERROR_EMPTY_CODE = 20;
	const ERROR_EMPTY_MESSAGE = 'empty-email-or-password';

	const ERROR_NOT_LOGGED_CODE = 40;
	const ERROR_NOT_LOGGED_MESSAGE = 'wrong-email-or-password';

	const SUCCESS_LOGGED_CODE = 60;
}

class LoginResponse extends Response {
	const EMAIL_PARAM = 'email';
	const PASSWORD_PARAM = 'password';

	private $email;
	private $passwordHash;

	public $username;
	public $token;

	public function __construct ($email, $password) {
		if(!$email || !$password) {
			$this->errorMessage = LoginCodesAndMessages::ERROR_EMPTY_MESSAGE;
			$this->statusCode = LoginCodesAndMessages::ERROR_EMPTY_CODE;
			return;
		}

		$this->email = $email;
		$this->passwordHash = hash('sha256', $password);

		$this->checkEmailAndPassword();
	}

	private function checkEmailAndPassword() {
		$data = Database::$db->fetch("SELECT username, token FROM users WHERE email = %s AND password = %s", $this->email, $this->passwordHash);
		if(isset($data->username)) {
			$this->statusCode = LoginCodesAndMessages::SUCCESS_LOGGED_CODE;
			$this->username = $data->username;
			$this->token = $data->token;
			return;
		}

		$this->statusCode = LoginCodesAndMessages::ERROR_NOT_LOGGED_CODE;
		$this->errorMessage = LoginCodesAndMessages::ERROR_NOT_LOGGED_MESSAGE;
	}
}

echo new LoginResponse(
	Utils::request(LoginResponse::EMAIL_PARAM),
	Utils::request(LoginResponse::PASSWORD_PARAM)
);
