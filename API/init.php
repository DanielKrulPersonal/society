<?php
require_once 'index.local.php';
require_once $GLOBALS['settings']['libs_dir'] . 'dibi.phar';
require_once 'Response.php';
require_once 'Utils.php';

header('Access-Control-Allow-Origin: *');

class Database
{
	/** @var Dibi\Connection */
	public static $db;
	public static function initialize()
	{
		$dibiConfig = [
			'driver' => $GLOBALS['db']['driver'],
			'host' => $GLOBALS['db']['host'],
			'username' => $GLOBALS['db']['username'],
			'database' => $GLOBALS['db']['database'],
			'password' => $GLOBALS['db']['password'],
			'port' => $GLOBALS['db']['port']
		];
		self::$db = new Dibi\Connection($dibiConfig);
	}
}
Database::initialize();