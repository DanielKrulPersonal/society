<?php
require_once '../init.php';

class ReviewListCodesAndMessages {
	const ERROR_NO_DATA_CODE = 20;
	const ERROR_NO_DATA_MESSAGE = 'no-reviews';

	const SUCCESS_CODE = 60;
}

class ReviewListResponse extends Response {
	const LIMIT_PARAM = 'limit';
	const OFFSET_PARAM = 'offset';
	const LANG_PARAM = 'lang';

	private $limit;
	private $offset;
	private $lang;

	public $data = [];

	public function __construct ($limit, $offset, $lang) {
		$this->limit = (int)$limit;
		$this->offset = (int)$offset;
		$this->lang = (string)$lang;

		$this->getData();
		return $this->data;
	}

	private function getData() {
		$data = Database::$db->fetchAll("
SELECT r.id, r.title, r.image, ROUND(AVG(r_i.stars)) as stars FROM reviews r
LEFT JOIN reviews_items r_i ON (r_i.reviews_id = r.id)
WHERE r.enabled = true AND r.lang = %s
GROUP BY r.id
ORDER BY r.sponsored ASC, stars DESC NULLS LAST, created_at DESC, title DESC
LIMIT ? OFFSET ?
",$this->lang, $this->limit, $this->offset);
		$this->data = (array)$data;

		if(!count($this->data)) {
			$this->statusCode = ReviewListCodesAndMessages::ERROR_NO_DATA_CODE;
			$this->errorMessage = ReviewListCodesAndMessages::ERROR_NO_DATA_MESSAGE;
			return;
		}

		foreach ($data as $item) {
			$item->image = json_decode($item->image);
		}

		$this->statusCode = ReviewListCodesAndMessages::SUCCESS_CODE;
	}
}

echo new ReviewListResponse(
	Utils::request(ReviewListResponse::LIMIT_PARAM),
	Utils::request(ReviewListResponse::OFFSET_PARAM),
	Utils::request(ReviewListResponse::LANG_PARAM)
);
