<?php
require_once '../init.php';

class AddItemCodesAndMessages
{
	const SUCCESS_CODE = 60;
}

class AddItemResponse extends Response
{
	const TITLE_PARAM = 'title';
	const HASHTAGS_PARAM = 'hashtags';
	const LANG_PARAM = 'lang';
	const USER_PARAM = 'user';
	const STARS_PARAM = 'stars';
	const TEXT_PARAM = 'text';
	const IMAGES_PARAM = 'images';

	/** @var string */
	private $title;
	/** @var string */
	private $hashtags;
	/** @var string  */
	private $lang;
	/** @var string */
	private $user;
	/** @var integer */
	private $stars;
	/** @var string */
	private $text;
	/** @var array */
	private $images;
	/** @var integer */
	public $reviewId;
	/** @var string[] */
	private $imageNames = [];

	public function __construct($title, $hashtags, $lang, $user, $stars, $text, $images)
	{
		$this->title = (string)$title;
		$this->hashtags = (string)$hashtags;
		$this->lang = (string)$lang;
		$this->user = (string)$user;
		$this->stars = (integer)$stars;
		$this->text = (string)$text;
		$this->images = json_decode($images);

		$this->processImages();
		$this->addItem();
	}

	private function processImages() {
		foreach ($this->images as $imageString) {
			$data = explode(',', $imageString);
			$imgData =  base64_decode($data[1]);
			preg_match("^data:([a-zA-Z]+/[a-zA-Z]+);base64\,([a-zA-Z0-9+\=/]+)$^", $imageString, $type);
			$extension = explode('/', $type[1]);
			$imageName = Utils::randomString(12) . '.' . $extension[1];
			file_put_contents('../images/' . $imageName, $imgData);

			$this->imageNames[] = $imageName;
		}
	}

	private function addItem()
	{
		$imageNamesEncoded = json_encode($this->imageNames);
		$data = Database::$db->fetch('
DROP TABLE IF EXISTS real_id;
SELECT set_limit(0.4);
CREATE TEMP TABLE data AS
SELECT
    similarity(title, %s) AS similarity,
    id
FROM
    reviews
WHERE
    title % %s
LIMIT 1;

DO $$
DECLARE
BEGIN
IF NOT EXISTS (SELECT id FROM data) THEN
   INSERT INTO reviews ([title], [lang], [hashtags], [created_at], [enabled], [sponsored], [user], [image]) VALUES (%s, %s, %s, now(), true, false, %sN, %s);
   INSERT INTO reviews_items ([reviews_id], [stars], [text]) VALUES ((SELECT last_value FROM reviews_id_seq), %i, %sN);
   CREATE TEMP TABLE real_id AS (SELECT last_value AS id FROM reviews_id_seq);
ELSE
   INSERT INTO reviews_items ([reviews_id], [stars], [text], [image]) VALUES ((SELECT id FROM data), %i, %sN, %s);
   CREATE TEMP TABLE real_id AS (SELECT id FROM data);
END IF;
END$$;

SELECT id FROM real_id;
', $this->title, $this->title, $this->title, $this->lang, $this->hashtags, $this->user, $imageNamesEncoded, $this->stars, $this->text, $this->stars, $this->text, $imageNamesEncoded);
		$this->reviewId = $data->id;
		$this->statusCode = AddItemCodesAndMessages::SUCCESS_CODE;
	}
}

echo new AddItemResponse(
	Utils::request(AddItemResponse::TITLE_PARAM),
	Utils::request(AddItemResponse::HASHTAGS_PARAM),
	Utils::request(AddItemResponse::LANG_PARAM),
	Utils::request(AddItemResponse::USER_PARAM),
	Utils::request(AddItemResponse::STARS_PARAM),
	Utils::request(AddItemResponse::TEXT_PARAM),
	Utils::request(AddItemResponse::IMAGES_PARAM)
);
