class BackgroundTasksWatcher {
	static tasksInstances: object = {};

	static registerTask (taskName: string, callback, interval: number) {
		BackgroundTasksWatcher.tasksInstances[taskName] = {
			'running': false,
			'functionInstance': function () {
				setTimeout(function () {
					if(BackgroundTasksWatcher.tasksInstances[taskName].running) {
						callback();
						setTimeout(BackgroundTasksWatcher.tasksInstances[taskName].functionInstance);
					}
				}, interval);
			}
		};
	}

	static runTask(taskName: string) {
		try {
			BackgroundTasksWatcher.tasksInstances[taskName].running = true;
			BackgroundTasksWatcher.tasksInstances[taskName].functionInstance();
		} catch (e) {
			console.error('Task not found: ' + taskName);
		}
	}

	static stopTask(taskName: string) {
		BackgroundTasksWatcher.tasksInstances[taskName].running = false;
	}
}