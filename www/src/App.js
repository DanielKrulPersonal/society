///  <reference path="_includes.ts"/>
var App = /** @class */ (function () {
    function App() {
        if (App.appRunned)
            return;
        App.appRunned = true;
        this.initialize();
    }
    App.prototype.processConfigFile = function (callback) {
        $.getJSON(App.ROOT_DIR + App.CONFIG_FILE, function (data) {
            App.config = data;
            App.config['lang'] = Utils.getBrowserLanguage();
            App.config['cache'] = data.cache;
            App.config['api-base-url'] = data['api-base-url'];
            App.config['default-limit'] = data['default-limit'];
            callback();
        });
    };
    App.prototype.initialize = function () {
        var _this = this;
        this.processConfigFile(function () {
            _this.registerModules();
        });
    };
    App.prototype.registerModules = function () {
        this.registerModule('system');
        this.registerModule('panel');
        this.registerModule('content');
        module('system').initialize();
        this.registerTasks();
    };
    App.prototype.registerTasks = function () {
        ForegroundTasksWatcher.registerTask('RegisterActions', function () {
            Template.registerActions();
        }, 200);
        ForegroundTasksWatcher.runTask('RegisterActions');
        BackgroundTasksWatcher.registerTask('CheckLogin', function () {
            CheckLogin.init();
        }, 1000);
        BackgroundTasksWatcher.runTask('CheckLogin');
    };
    App.prototype.registerModule = function (moduleName) {
        var modulePath = App.ROOT_DIR + 'modules/' + moduleName + '/' + moduleName + '.module.js';
        Utils.addJsToHead(modulePath);
        var ins = window[moduleName + 'ModuleInstancer']();
        ins.moduleName = moduleName;
        App.modulesInstances[moduleName] = ins;
    };
    App.isCache = function () {
        return App.config['cache'];
    };
    App.CONFIG_FILE = 'config.json';
    App.ROOT_DIR = 'src/';
    App.APP_SALT = 'ŘEŘICHOVÁ_LIMONÁDA_S_TĚŽIŠTĚM';
    App.modulesInstances = {};
    App.config = {};
    App.appRunned = false;
    return App;
}());
$(document).ready(function () {
    if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) {
        document.addEventListener('deviceready', function () {
            new App();
        });
        new App();
    }
    else {
        new App();
    }
});
function module(moduleName) {
    return App.modulesInstances[moduleName];
}
