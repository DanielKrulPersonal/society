var AppCache = /** @class */ (function () {
    function AppCache() {
    }
    AppCache.setItem = function (name, validTime, item) {
        AppCache.cachedItems[name] = {
            'validTo': (Math.round(Date.now() / 1000) + validTime) * 1000,
            'item': item
        };
        AppCache.save();
    };
    AppCache.save = function () {
        localStorage.setItem('cache', JSON.stringify(AppCache.cachedItems));
    };
    AppCache.reload = function () {
        var data = localStorage.getItem('cache');
        if (data)
            AppCache.cachedItems = JSON.parse(data);
    };
    AppCache.isValid = function (name) {
        AppCache.reload();
        try {
            if (Date.now() > AppCache.cachedItems[name].validTo) {
                return false;
            }
        }
        catch (e) {
            return false;
        }
        return true;
    };
    AppCache.getItem = function (name) {
        return AppCache.cachedItems[name].item;
    };
    AppCache.cachedItems = {};
    return AppCache;
}());
