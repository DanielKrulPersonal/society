///  <reference path="_includes.ts"/>

class Translate {
	static readonly LOCALES_DIR = 'locales/';

	/**
	 * @param {string} textId
	 * @param {string} moduleName
	 * @param callback
	 */
	static translate(textId: string, moduleName: string, callback): void {
		$.getJSON( App.ROOT_DIR + 'modules/' + moduleName + '/' + Translate.LOCALES_DIR + App.config['lang'] + '/strings.json', function(data) {
			callback(data[textId]);
		}).fail(function(e) {
			console.error('Translate not found: ' + textId + ' in module: ' + moduleName);
		});
	}
}

/**
 * @param {string} textId
 * @param {string} moduleName
 * @param callback
 */
function __(textId, moduleName: string, callback): void {
	return Translate.translate(textId, moduleName, callback);
}