///  <reference path="_includes.ts"/>
declare var CryptoJS: any;

class ApiConnector {
	static readonly USER_PART = 'user/';
	static readonly REVIEWS_PART = 'reviews/';
	
	static readonly LOGIN_ACTION = 'login/';
	static readonly ADD_ITEM_ACTION = 'addItem/';
	static readonly LIST_ACTION = 'list/';
	static readonly REGISTER_ACTION = 'register/';
	static readonly LOGIN_SUCCESS_LOGGED_CODE = 60;
	static readonly LOGIN_NOT_LOGGED_CODE = 40;
	static readonly SUCCESS_REGISTERED_CODE = 60;
	static readonly ERROR_USERNAME_EXISTS_CODE = 40;
	static readonly ERROR_EMAIL_EXISTS_CODE = 20;

	static loginAction(params: object, rememberLogin: boolean, callback, notSave: boolean = false) {
		$.post(App.config['api-base-url'] + ApiConnector.USER_PART + ApiConnector.LOGIN_ACTION, params, function (e) {
			switch(e.statusCode) {
				case ApiConnector.LOGIN_SUCCESS_LOGGED_CODE: // login OK
					if(!notSave) {
						if(rememberLogin) {
							localStorage.setItem('login_data', CryptoJS.AES.encrypt(JSON.stringify(params), e.token + App.APP_SALT).toString());
							localStorage.setItem('token', e.token);
						} else {
							sessionStorage.setItem('login_data', CryptoJS.AES.encrypt(JSON.stringify(params), e.token + App.APP_SALT).toString());
							sessionStorage.setItem('token', e.token);
						}
					}

					break;
				case ApiConnector.LOGIN_NOT_LOGGED_CODE: // wrong email or password
					break;
			}

			callback(e);
		}, 'json').fail(function () {
			console.error('API login failed');
		});
	}
	
	static registerAction(params: object, callback) {
		$.post(App.config['api-base-url'] + ApiConnector.USER_PART + ApiConnector.REGISTER_ACTION, params, function (e) {
			callback(e);
		}, 'json').fail(function () {
			console.error('API register failed');
		});
	}
	
	static getList(params: object, callback) {
		$.post(App.config['api-base-url'] + ApiConnector.REVIEWS_PART + ApiConnector.LIST_ACTION, params, function (e) {
			callback(e);
		}, 'json').fail(function () {
			console.error('API getList failed');
		});
	}
	
	static addItem(params: object, callback) {
		$.post(App.config['api-base-url'] + ApiConnector.REVIEWS_PART + ApiConnector.ADD_ITEM_ACTION, params, function (e) {
			callback(e);
		}, 'json').fail(function () {
			console.error('API addItem failed');
		});
	}
}