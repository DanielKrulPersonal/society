///  <reference path="../_includes.ts"/>
var CheckLogin = /** @class */ (function () {
    function CheckLogin() {
    }
    CheckLogin.init = function () {
        CheckLogin.sideMenuLoginOrProfileHref = $('#js-sideMenuLoginOrProfileHref');
        var _this = this;
        var loginData = localStorage.getItem('login_data') || sessionStorage.getItem('login_data');
        var token = localStorage.getItem('token') || sessionStorage.getItem('token');
        if (token && loginData) {
            var loginDataParsed = JSON.parse(CryptoJS.AES.decrypt(loginData, token + App.APP_SALT).toString(CryptoJS.enc.Utf8));
            ApiConnector.loginAction(loginDataParsed, null, function (data) {
                if (data.statusCode === ApiConnector.LOGIN_SUCCESS_LOGGED_CODE) { // login OK
                    CheckLogin.sideMenuLoginOrProfileHref.parent().data('action', 'contentUser');
                    __('side-menu-loginOrProfile-href-logged', 'panel', function (e) {
                        CheckLogin.sideMenuLoginOrProfileHref.html(e);
                    });
                }
            }, true);
        }
        else {
            localStorage.removeItem('login_data');
            sessionStorage.removeItem('login_data');
            localStorage.removeItem('token');
            sessionStorage.removeItem('token');
            CheckLogin.sideMenuLoginOrProfileHref.parent().data('action', 'contentLogin');
            __('side-menu-loginOrProfile-href', 'panel', function (e) {
                CheckLogin.sideMenuLoginOrProfileHref.html(e);
            });
        }
    };
    return CheckLogin;
}());
