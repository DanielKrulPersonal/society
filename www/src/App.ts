///  <reference path="_includes.ts"/>

class App {
	static readonly CONFIG_FILE: string = 'config.json';
	static readonly ROOT_DIR: string = 'src/';
	static readonly APP_SALT: string = 'ŘEŘICHOVÁ_LIMONÁDA_S_TĚŽIŠTĚM';
	static modulesInstances: object = {};
	static config: object = {};
	static appRunned: boolean = false;

	public constructor() {
		if(App.appRunned) return;
		App.appRunned = true;

		this.initialize();
	}

	private processConfigFile(callback): void {
		$.getJSON( App.ROOT_DIR + App.CONFIG_FILE, function(data) {
			App.config = data;
			App.config['lang'] = Utils.getBrowserLanguage();
			App.config['cache'] = data.cache;
			App.config['api-base-url'] = data['api-base-url'];
			App.config['default-limit'] = data['default-limit'];

			callback();
		});
	}

	public initialize (): void {
		let _this = this;

		this.processConfigFile(function () {
			_this.registerModules();
		});
	}

	private registerModules() {
		this.registerModule('system');

		this.registerModule('panel');
		this.registerModule('content');

		module('system').initialize();
		this.registerTasks();
	}

	private registerTasks() {
		ForegroundTasksWatcher.registerTask('RegisterActions', function () {
			Template.registerActions();
		}, 200);
		ForegroundTasksWatcher.runTask('RegisterActions');

		BackgroundTasksWatcher.registerTask('CheckLogin', function () {
			CheckLogin.init();
		}, 1000);
		BackgroundTasksWatcher.runTask('CheckLogin');
	}

	private registerModule(moduleName: string): void {
		let modulePath: string  = App.ROOT_DIR + 'modules/' + moduleName + '/' + moduleName + '.module.js';

		Utils.addJsToHead(modulePath);
		let ins = window[moduleName + 'ModuleInstancer']();
		ins.moduleName = moduleName;
		App.modulesInstances[moduleName] = ins;
	}

	public static isCache(): boolean {
		return App.config['cache'];
	}
}

$(document).ready(function () {
	if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) {
		document.addEventListener('deviceready', function () {
			new App();
		});

		new App();
	} else {
		new App();
	}
});



function module(moduleName: string) {
	return App.modulesInstances[moduleName];
}
