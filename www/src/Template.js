///  <reference path="_includes.ts"/>
var Template = /** @class */ (function () {
    /**
     * @param {string} templateName
     * @param {string} templatePath
     * @param {string} classNames
     * @param {string} moduleName
     */
    function Template(templateName, templatePath, classNames, moduleName) {
        if (classNames === void 0) { classNames = ''; }
        this.rendered = false;
        this.templateName = templateName;
        this.templatePath = templatePath;
        this.classNames = classNames;
        this.templateId = Utils.randomString(15, 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') + '_template';
        this.controllerId = Utils.randomString(15, 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') + '_controller';
        this.moduleName = moduleName;
        var instanceName = this.templateName + '__' + this.moduleName;
        this.htmlContent = $('<div data-template="' + this.templateName + '" class="' + this.classNames + '" id="' + this.templateId + '"></div>');
        Template.templatesInstances[instanceName] = this;
    }
    /**
     * @param {string} templateName
     * @param {string} moduleName
     * @returns {Template}
     */
    Template.getTemplate = function (templateName, moduleName) {
        var instanceName = templateName + '__' + moduleName;
        return Template.templatesInstances[instanceName];
    };
    Template.prototype.deleteHtml = function () {
        $('#' + this.templateId).remove();
    };
    /**
     * @param callback
     */
    Template.prototype.render = function (callback) {
        var _this = this;
        return Utils.getFileContent(this.templatePath, function (e) {
            if (!_this.rendered) {
                _this.htmlContent.append(e);
                _this.rendered = true;
            }
            callback(_this.htmlContent);
        });
    };
    /**
     * @param {Template} templateInstance
     */
    Template.prototype.add = function (templateInstance) {
        var _this = this;
        templateInstance.render(function (e) {
            _this.htmlContent.append(e);
        });
    };
    Template.prototype.addController = function () {
        this.controllerPath = App.ROOT_DIR + 'modules/' + this.moduleName + '/controllers/' + this.templateName + '.js';
        var controllerElement = this.htmlContent.find('#' + this.controllerId);
        if (!controllerElement.length) {
            this.htmlContent.append('<script id="' + this.controllerId + '" type="text/javascript" src="' + this.controllerPath + '" ></script>');
        }
    };
    Template.registerActions = function () {
        var el = $('*[data-action][data-module]');
        var actionName, moduleName;
        el.click(function () {
            actionName = $(this).data('action');
            moduleName = $(this).data('module');
            try {
                runAction(actionName, moduleName);
            }
            catch (e) {
                console.error('Action error: ' + e);
            }
        });
        if (!Template.redirected) {
            var url = new URL(window.location.href);
            var urlAction = url.searchParams.get('action');
            var urlModule = url.searchParams.get('module');
            if (urlAction && urlModule)
                runAction(urlAction, urlModule);
        }
    };
    Template.templatesInstances = {};
    Template.redirected = false;
    return Template;
}());
/**
 * @param {string} templateName
 * @param {string} moduleName
 * @returns {Template}
 */
function template(templateName, moduleName) {
    return Template.getTemplate(templateName, moduleName);
}
