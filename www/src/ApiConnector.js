///  <reference path="_includes.ts"/>
var ApiConnector = /** @class */ (function () {
    function ApiConnector() {
    }
    ApiConnector.loginAction = function (params, rememberLogin, callback, notSave) {
        if (notSave === void 0) { notSave = false; }
        $.post(App.config['api-base-url'] + ApiConnector.USER_PART + ApiConnector.LOGIN_ACTION, params, function (e) {
            switch (e.statusCode) {
                case ApiConnector.LOGIN_SUCCESS_LOGGED_CODE: // login OK
                    if (!notSave) {
                        if (rememberLogin) {
                            localStorage.setItem('login_data', CryptoJS.AES.encrypt(JSON.stringify(params), e.token + App.APP_SALT).toString());
                            localStorage.setItem('token', e.token);
                        }
                        else {
                            sessionStorage.setItem('login_data', CryptoJS.AES.encrypt(JSON.stringify(params), e.token + App.APP_SALT).toString());
                            sessionStorage.setItem('token', e.token);
                        }
                    }
                    break;
                case ApiConnector.LOGIN_NOT_LOGGED_CODE: // wrong email or password
                    break;
            }
            callback(e);
        }, 'json').fail(function () {
            console.error('API login failed');
        });
    };
    ApiConnector.registerAction = function (params, callback) {
        $.post(App.config['api-base-url'] + ApiConnector.USER_PART + ApiConnector.REGISTER_ACTION, params, function (e) {
            callback(e);
        }, 'json').fail(function () {
            console.error('API register failed');
        });
    };
    ApiConnector.getList = function (params, callback) {
        $.post(App.config['api-base-url'] + ApiConnector.REVIEWS_PART + ApiConnector.LIST_ACTION, params, function (e) {
            callback(e);
        }, 'json').fail(function () {
            console.error('API getList failed');
        });
    };
    ApiConnector.addItem = function (params, callback) {
        $.post(App.config['api-base-url'] + ApiConnector.REVIEWS_PART + ApiConnector.ADD_ITEM_ACTION, params, function (e) {
            callback(e);
        }, 'json').fail(function () {
            console.error('API addItem failed');
        });
    };
    ApiConnector.USER_PART = 'user/';
    ApiConnector.REVIEWS_PART = 'reviews/';
    ApiConnector.LOGIN_ACTION = 'login/';
    ApiConnector.ADD_ITEM_ACTION = 'addItem/';
    ApiConnector.LIST_ACTION = 'list/';
    ApiConnector.REGISTER_ACTION = 'register/';
    ApiConnector.LOGIN_SUCCESS_LOGGED_CODE = 60;
    ApiConnector.LOGIN_NOT_LOGGED_CODE = 40;
    ApiConnector.SUCCESS_REGISTERED_CODE = 60;
    ApiConnector.ERROR_USERNAME_EXISTS_CODE = 40;
    ApiConnector.ERROR_EMAIL_EXISTS_CODE = 20;
    return ApiConnector;
}());
