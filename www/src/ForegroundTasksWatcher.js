var ForegroundTasksWatcher = /** @class */ (function () {
    function ForegroundTasksWatcher() {
    }
    ForegroundTasksWatcher.registerTask = function (taskName, callback, interval) {
        ForegroundTasksWatcher.tasksInstances[taskName] = {
            'running': false,
            'functionInstance': function () {
                setTimeout(function () {
                    if (ForegroundTasksWatcher.tasksInstances[taskName].running) {
                        callback();
                        requestAnimationFrame(ForegroundTasksWatcher.tasksInstances[taskName].functionInstance);
                    }
                }, interval);
            }
        };
    };
    ForegroundTasksWatcher.runTask = function (taskName) {
        try {
            ForegroundTasksWatcher.tasksInstances[taskName].running = true;
            ForegroundTasksWatcher.tasksInstances[taskName].functionInstance();
        }
        catch (e) {
            console.error('Task not found: ' + taskName);
        }
    };
    ForegroundTasksWatcher.stopTask = function (taskName) {
        ForegroundTasksWatcher.tasksInstances[taskName].running = false;
    };
    ForegroundTasksWatcher.tasksInstances = {};
    return ForegroundTasksWatcher;
}());
