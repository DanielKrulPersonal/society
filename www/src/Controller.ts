///  <reference path="_includes.ts"/>

class Controller {
	public registerController(controllerName: string, moduleName: string) {
		Utils.addJsToHead(App.ROOT_DIR + 'modules/' + moduleName + '/models/' + controllerName + '.js');
	}
}