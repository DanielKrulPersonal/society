class ForegroundTasksWatcher {
	static tasksInstances: object = {};

	static registerTask (taskName: string, callback, interval: number) {
		ForegroundTasksWatcher.tasksInstances[taskName] = {
			'running': false,
			'functionInstance': function () {
				setTimeout(function () {
					if(ForegroundTasksWatcher.tasksInstances[taskName].running) {
						callback();
						requestAnimationFrame(ForegroundTasksWatcher.tasksInstances[taskName].functionInstance);
					}
				}, interval);
			}
		};
	}

	static runTask(taskName: string) {
		try {
			ForegroundTasksWatcher.tasksInstances[taskName].running = true;
			ForegroundTasksWatcher.tasksInstances[taskName].functionInstance();
		} catch (e) {
			console.error('Task not found: ' + taskName);
		}
	}

	static stopTask(taskName: string) {
		ForegroundTasksWatcher.tasksInstances[taskName].running = false;
	}
}