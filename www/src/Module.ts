///  <reference path="_includes.ts"/>
class Module {
	public moduleName: string;
	public parentModuleName: string;
	public templatesPath: string;
	public pathModuleName: string;

	static actionsInstances: object = {};
	static lastCalledAction: object = {};

	public initialize(): void {
		this.pathModuleName = this.parentModuleName ? this.parentModuleName : this.moduleName;
		this.templatesPath = App.ROOT_DIR + 'modules/' + this.pathModuleName + '/templates';
	}

	/**
	 * @param {string} templateName
	 * @param {string} classNames
	 * @returns {Template}
	 */
	public newTemplate(templateName: string, classNames: string = '') {
		return new Template(templateName, this.templatesPath + '/' + templateName + '.html', classNames, this.pathModuleName);
	}

	/**
	 * @param {string} actionName
	 * @param method
	 */
	public setAction(actionName: string, method: any): void {
		Module.actionsInstances[actionName + '__' + this.moduleName] = {'method': method, 'caller': this};
	}
}

/**
 * @param {string} actionName
 * @param {string} moduleName
 * @returns {object}
 */
function action(actionName: string, moduleName: string): object {
	return Module.actionsInstances[actionName + '__' + moduleName];
}

/**
 *
 * @param {string} actionName
 * @param {string} moduleName
 * @param {boolean} inUrl
 * @param {object} payload
 */
function runAction(actionName: string, moduleName: string, inUrl: boolean = true, payload: object = {}) {
	let emptyFunc = function () {};
	if(Module.lastCalledAction['moduleName'] === moduleName && Module.lastCalledAction['moduleAction'] === actionName) return {'method': emptyFunc};
	Module.lastCalledAction = {'moduleName': moduleName, 'moduleAction': actionName};

	if(inUrl) {
		let urlData: object = {
			'action': actionName,
			'module': moduleName
		};
		$.extend(urlData, payload);

		history.pushState({}, null, '?' +  $.param(urlData));
	}
	action(actionName, moduleName)['method']();
}