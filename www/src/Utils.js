///  <reference path="_includes.ts"/>
var Utils = /** @class */ (function () {
    function Utils() {
    }
    Utils.addJsToHead = function (location) {
        if (!App.isCache())
            location += '?cache=' + Date.now();
        $('head').append('<script type="text/javascript" src="' + location + '"></script>');
    };
    Utils.getFileContent = function (fileLocation, callback) {
        $.get(fileLocation, function (e) {
            callback(e);
        });
    };
    /**
     * @param {number} len
     * @param {string} charSet
     * @returns {string}
     */
    Utils.randomString = function (len, charSet) {
        charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var randomString = '';
        var i;
        for (i = 0; i < len; i++) {
            var randomPoz = Math.floor(Math.random() * charSet.length);
            randomString += charSet.substring(randomPoz, randomPoz + 1);
        }
        return randomString;
    };
    /**
     * Přidá HTML element nebo kód do body
     * @param {string} html
     */
    Utils.addHtmlToBody = function (html) {
        $('body').append(html);
    };
    /**
     * @returns {string}
     */
    Utils.getBrowserLanguage = function () {
        // @ts-ignore
        var _a = (((navigator.userLanguage || navigator.language).replace('-', '_')).toLowerCase()).split('_'), lang = _a[0], locale = _a[1];
        return lang;
    };
    /**
     * @param {string} selector
     * @param callback
     */
    Utils.waitForElement = function (selector, callback) {
        if ($(selector).length) {
            callback();
        }
        else {
            setTimeout(function () {
                Utils.waitForElement(selector, callback);
            }, 5);
        }
    };
    /**
     * @param {string} key
     * @returns {string}
     */
    Utils.removeUrlParam = function (key) {
        if (!key)
            return;
        var url = document.location.href;
        var urlparts = url.split('?');
        if (urlparts.length >= 2) {
            var urlBase = urlparts.shift();
            var queryString = urlparts.join("?");
            var prefix = encodeURIComponent(key) + '=';
            var pars = queryString.split(/[&;]/g);
            for (var i = pars.length; i-- > 0;)
                if (pars[i].lastIndexOf(prefix, 0) !== -1)
                    pars.splice(i, 1);
            url = urlBase + '?' + pars.join('&');
            window.history.pushState('', document.title, url);
        }
        return url;
    };
    return Utils;
}());
