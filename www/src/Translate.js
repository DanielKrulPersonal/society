///  <reference path="_includes.ts"/>
var Translate = /** @class */ (function () {
    function Translate() {
    }
    /**
     * @param {string} textId
     * @param {string} moduleName
     * @param callback
     */
    Translate.translate = function (textId, moduleName, callback) {
        $.getJSON(App.ROOT_DIR + 'modules/' + moduleName + '/' + Translate.LOCALES_DIR + App.config['lang'] + '/strings.json', function (data) {
            callback(data[textId]);
        }).fail(function (e) {
            console.error('Translate not found: ' + textId + ' in module: ' + moduleName);
        });
    };
    Translate.LOCALES_DIR = 'locales/';
    return Translate;
}());
/**
 * @param {string} textId
 * @param {string} moduleName
 * @param callback
 */
function __(textId, moduleName, callback) {
    return Translate.translate(textId, moduleName, callback);
}
