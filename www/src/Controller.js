///  <reference path="_includes.ts"/>
var Controller = /** @class */ (function () {
    function Controller() {
    }
    Controller.prototype.registerController = function (controllerName, moduleName) {
        Utils.addJsToHead(App.ROOT_DIR + 'modules/' + moduleName + '/models/' + controllerName + '.js');
    };
    return Controller;
}());
