var BackgroundTasksWatcher = /** @class */ (function () {
    function BackgroundTasksWatcher() {
    }
    BackgroundTasksWatcher.registerTask = function (taskName, callback, interval) {
        BackgroundTasksWatcher.tasksInstances[taskName] = {
            'running': false,
            'functionInstance': function () {
                setTimeout(function () {
                    if (BackgroundTasksWatcher.tasksInstances[taskName].running) {
                        callback();
                        setTimeout(BackgroundTasksWatcher.tasksInstances[taskName].functionInstance);
                    }
                }, interval);
            }
        };
    };
    BackgroundTasksWatcher.runTask = function (taskName) {
        try {
            BackgroundTasksWatcher.tasksInstances[taskName].running = true;
            BackgroundTasksWatcher.tasksInstances[taskName].functionInstance();
        }
        catch (e) {
            console.error('Task not found: ' + taskName);
        }
    };
    BackgroundTasksWatcher.stopTask = function (taskName) {
        BackgroundTasksWatcher.tasksInstances[taskName].running = false;
    };
    BackgroundTasksWatcher.tasksInstances = {};
    return BackgroundTasksWatcher;
}());
