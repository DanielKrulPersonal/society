///  <reference path="_includes.ts"/>
class Template {
	static templatesInstances = {};
	readonly templateName: string;
	readonly templatePath: string;
	readonly classNames: string;
	readonly templateId: string;
	readonly moduleName: string;
	readonly htmlContent: JQuery;
	readonly controllerId: string;
	static redirected: boolean = false;

	private rendered: boolean = false;
	private controllerPath: string;

	/**
	 * @param {string} templateName
	 * @param {string} templatePath
	 * @param {string} classNames
	 * @param {string} moduleName
	 */
	public constructor(templateName: string, templatePath: string, classNames: string = '', moduleName: string) {
		this.templateName = templateName;
		this.templatePath = templatePath;
		this.classNames = classNames;
		this.templateId = Utils.randomString(15, 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') + '_template';
		this.controllerId = Utils.randomString(15, 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') + '_controller';
		this.moduleName = moduleName;

		let instanceName = this.templateName + '__' + this.moduleName;
		this.htmlContent = $('<div data-template="'+ this.templateName +'" class="'+ this.classNames +'" id="'+ this.templateId +'"></div>');

		Template.templatesInstances[instanceName] = this;
	}

	/**
	 * @param {string} templateName
	 * @param {string} moduleName
	 * @returns {Template}
	 */
	static getTemplate(templateName: string, moduleName: string): Template {
		let instanceName = templateName + '__' + moduleName;
		return Template.templatesInstances[instanceName];
	}

	public deleteHtml(): void {
		$('#' + this.templateId).remove();
	}

	/**
	 * @param callback
	 */
	public render(callback) {
		let _this = this;

		return Utils.getFileContent(this.templatePath, function(e) {
			if(!_this.rendered) {
				_this.htmlContent.append(e);
				_this.rendered = true;
			}

			callback(_this.htmlContent);
		});
	}

	/**
	 * @param {Template} templateInstance
	 */
	public add(templateInstance: Template): void {
		let _this = this;

		templateInstance.render(function (e) {
			_this.htmlContent.append(e);
		});
	}

	public addController(): void {
		this.controllerPath = App.ROOT_DIR + 'modules/' + this.moduleName + '/controllers/' + this.templateName + '.js';

		let controllerElement = this.htmlContent.find('#' + this.controllerId);
		if(!controllerElement.length) {
			this.htmlContent.append('<script id="'+ this.controllerId +'" type="text/javascript" src="'+ this.controllerPath +'" ></script>');
		}
	}

	static registerActions(): void {
		let el: JQuery = $('*[data-action][data-module]');
		let actionName: string, moduleName: string;

		el.click(function () {
			actionName = $(this).data('action');
			moduleName = $(this).data('module');
			try {
				runAction(actionName, moduleName);
			} catch (e) {
				console.error('Action error: ' + e);
			}
		});

		if(!Template.redirected) {
			let url = new URL(window.location.href);
			let urlAction = url.searchParams.get('action');
			let urlModule = url.searchParams.get('module');

			if(urlAction && urlModule) runAction(urlAction, urlModule);
		}
	}
}

/**
 * @param {string} templateName
 * @param {string} moduleName
 * @returns {Template}
 */
function template(templateName: string, moduleName: string): Template {
	return Template.getTemplate(templateName, moduleName);
}