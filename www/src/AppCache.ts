class AppCache {
	static cachedItems: object = {};

	static setItem(name: string, validTime: number, item: any) {
		AppCache.cachedItems[name] = {
			'validTo': (Math.round(Date.now() / 1000) + validTime) * 1000,
			'item': item
		};

		AppCache.save();
	}

	private static save() {
		localStorage.setItem('cache', JSON.stringify(AppCache.cachedItems));
	}

	private static reload() {
		let data = localStorage.getItem('cache');
		if(data) AppCache.cachedItems = JSON.parse(data);
	}

	static isValid(name: string) {
		AppCache.reload();

		try {
			if(Date.now() > AppCache.cachedItems[name].validTo) {
				return false;
			}
		} catch (e) {
			return false;
		}

		return true;
	}

	static getItem(name: string) {
		return AppCache.cachedItems[name].item;
	}
}