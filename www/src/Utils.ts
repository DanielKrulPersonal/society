///  <reference path="_includes.ts"/>

class Utils {
	static addJsToHead(location: string) {
		if (!App.isCache()) location += '?cache=' + Date.now();
		$('head').append('<script type="text/javascript" src="' + location + '"></script>');
	}

	static getFileContent(fileLocation: string, callback) {
		$.get(fileLocation, function (e) {
			callback(e);
		});
	}

	/**
	 * @param {number} len
	 * @param {string} charSet
	 * @returns {string}
	 */
	static randomString(len: number, charSet: string): string {
		charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		let randomString = '';
		let i;
		for (i = 0; i < len; i++) {
			let randomPoz = Math.floor(Math.random() * charSet.length);
			randomString += charSet.substring(randomPoz, randomPoz + 1);
		}
		return randomString;
	}

	/**
	 * Přidá HTML element nebo kód do body
	 * @param {string} html
	 */
	static addHtmlToBody(html: string) {
		$('body').append(html);
	}

	/**
	 * @returns {string}
	 */
	static getBrowserLanguage(): string {
		// @ts-ignore
		let [lang, locale] = (((navigator.userLanguage || navigator.language).replace('-', '_')).toLowerCase()).split('_');
		return lang;
	}

	/**
	 * @param {string} selector
	 * @param callback
	 */
	static waitForElement(selector: string, callback) {
		if ($(selector).length) {
			callback();
		} else {
			setTimeout(function () {
				Utils.waitForElement(selector, callback);
			}, 5);
		}
	}

	/**
	 * @param {string} key
	 * @returns {string}
	 */
	static removeUrlParam(key: string) {
		if (!key) return;

		let url = document.location.href;
		let urlparts = url.split('?');

		if (urlparts.length >= 2) {
			let urlBase = urlparts.shift();
			let queryString = urlparts.join("?");

			let prefix = encodeURIComponent(key) + '=';
			let pars = queryString.split(/[&;]/g);
			for (let i = pars.length; i-- > 0;)
				if (pars[i].lastIndexOf(prefix, 0) !== -1)
					pars.splice(i, 1);
			url = urlBase + '?' + pars.join('&');
			window.history.pushState('', document.title, url);

		}
		return url;
	}
}