var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///  <reference path="../../_includes.ts"/>
var systemModule = /** @class */ (function (_super) {
    __extends(systemModule, _super);
    function systemModule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    systemModule.prototype.initialize = function () {
        _super.prototype.initialize.call(this);
        var systemTemplate = _super.prototype.newTemplate.call(this, 'system', 'd-flex flex-column h-100');
        module('panel').initialize();
        module('content').initialize();
        systemTemplate.render(function (e) {
            Utils.addHtmlToBody(e);
        });
    };
    return systemModule;
}(Module));
function systemModuleInstancer() {
    return new systemModule();
}
