///  <reference path="../../_includes.ts"/>
class systemModule extends Module {
	public initialize() {
		super.initialize();

		let systemTemplate: Template = super.newTemplate('system', 'd-flex flex-column h-100');

		module('panel').initialize();
		module('content').initialize();

		systemTemplate.render(function (e) {
			Utils.addHtmlToBody(e);
		});
	}
}

function systemModuleInstancer() {
	return new systemModule();
}