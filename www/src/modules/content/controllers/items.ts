///  <reference path="../../../_includes.ts"/>
///  <reference path="../models/ReviewsList.ts"/>

class itemsController extends Controller {

	private itemsSpace: JQuery;
	private topPanelMainTemplateSpace: JQuery;
	private response: object;
	private reviewAddedSuccessAlert: JQuery;
	private url: URL = new URL(window.location.href);

	public initialize() {
		super.registerController('ReviewsList', 'content');

		let _this = this;
		this.topPanelMainTemplateSpace = $('#js-topPanelMainTemplateSpace');
		this.itemsSpace = $('#js-itemsSpace');
		this.reviewAddedSuccessAlert = $('#js-reviewAdded');

		if(this.url.searchParams.get('showAdded') && this.url.searchParams.get('reviewId')) {
			console.log(this.url.searchParams.get('reviewId'));
			this.reviewAddedSuccessAlert.show();
			Utils.removeUrlParam('showAdded');
			Utils.removeUrlParam('reviewId');
		} else {
			this.reviewAddedSuccessAlert.hide();
		}

		this.topPanelMainTemplateSpace.parent().show();
		this.itemsSpace.html('');

		_this.updateData();

		this.registerEvents();
		this.translateText();
	}

	private updateData() {
		let _this = this;

		ReviewsList.getList(App.config['default-limit'], 0, function (data) {
			_this.response = data;

			_this.handleResponse();
		});
	}

	private handleResponse() {
		let _this = this;

		this.response['data'].forEach(function(item) {
			let img: JQuery = $('<img class="cursor-pointer" src="" />');
			img.attr('src', App.config['api-base-url'] + 'images/' + item.image[0]);
			_this.itemsSpace.append(img);
		});
	}

	private registerEvents() {
		$('.alert').on('close.bs.alert', function () {
			$(this).hide();
			return false;
		});
	}

	private translateText() {
		let _this = this;

		__('review-added', 'content', function (e) {
			_this.reviewAddedSuccessAlert.find('.text').html(e);
		});
	}
}

new itemsController().initialize();

