///  <reference path="../../../_includes.ts"/>
///  <reference path="../models/ReviewsList.ts"/>
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var itemsController = /** @class */ (function (_super) {
    __extends(itemsController, _super);
    function itemsController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.url = new URL(window.location.href);
        return _this;
    }
    itemsController.prototype.initialize = function () {
        _super.prototype.registerController.call(this, 'ReviewsList', 'content');
        var _this = this;
        this.topPanelMainTemplateSpace = $('#js-topPanelMainTemplateSpace');
        this.itemsSpace = $('#js-itemsSpace');
        this.reviewAddedSuccessAlert = $('#js-reviewAdded');
        if (this.url.searchParams.get('showAdded') && this.url.searchParams.get('reviewId')) {
            console.log(this.url.searchParams.get('reviewId'));
            this.reviewAddedSuccessAlert.show();
            Utils.removeUrlParam('showAdded');
            Utils.removeUrlParam('reviewId');
        }
        else {
            this.reviewAddedSuccessAlert.hide();
        }
        this.topPanelMainTemplateSpace.parent().show();
        this.itemsSpace.html('');
        _this.updateData();
        this.registerEvents();
        this.translateText();
    };
    itemsController.prototype.updateData = function () {
        var _this = this;
        ReviewsList.getList(App.config['default-limit'], 0, function (data) {
            _this.response = data;
            _this.handleResponse();
        });
    };
    itemsController.prototype.handleResponse = function () {
        var _this = this;
        this.response['data'].forEach(function (item) {
            var img = $('<img class="cursor-pointer" src="" />');
            img.attr('src', App.config['api-base-url'] + 'images/' + item.image[0]);
            _this.itemsSpace.append(img);
        });
    };
    itemsController.prototype.registerEvents = function () {
        $('.alert').on('close.bs.alert', function () {
            $(this).hide();
            return false;
        });
    };
    itemsController.prototype.translateText = function () {
        var _this = this;
        __('review-added', 'content', function (e) {
            _this.reviewAddedSuccessAlert.find('.text').html(e);
        });
    };
    return itemsController;
}(Controller));
new itemsController().initialize();
