///  <reference path="../../../_includes.ts"/>
///  <reference path="../models/AddItem.ts"/>

class addItemController extends Controller {
	private topPanelMainTemplateSpace: JQuery;
	private topPanelHeight: number;
	private contextMenu: JQuery;
	private addItemForm: JQuery;
	private itemTitle: JQuery;
	private itemHashtags: JQuery;
	private ratingStar: JQuery;
	private choosedStar: number = null;
	private emptyStarErrorAlert: JQuery;
	private text: JQuery;
	private dropZone: JQuery;
	private uploadImagesFileInput: JQuery;
	private uploadedImagesList: string[] = [];
	private noImagesErrorAlert: JQuery;
	
	public initialize() {
		super.registerController('AddItem', 'content');

		this.topPanelMainTemplateSpace = $('#js-topPanelMainTemplateSpace');
		this.topPanelHeight = this.topPanelMainTemplateSpace.parent().outerHeight();

		this.topPanelMainTemplateSpace.parent().hide();
		this.contextMenu = $('#js-contextMenu');
		this.addItemForm = $('#js-addItemForm');
		this.itemTitle = $('#js-title');
		this.itemHashtags = $('#js-hashtags');
		this.ratingStar = $('.js-rating-star');
		this.emptyStarErrorAlert = $('#js-emptyStar');
		this.text = $('#js-text');
		this.dropZone = $('#js-drop-zone');
		this.uploadImagesFileInput = $('#js-upload-images');
		this.noImagesErrorAlert = $('#js-no-images');

		this.contextMenu.height(this.topPanelHeight + 'px');
		this.ratingStar.find('.fa').switchClass('fa-star', 'fa-star-o');
		
		this.emptyStarErrorAlert.hide();
		this.noImagesErrorAlert.hide();

		this.registerEvents();
		this.translateText();
	}
	
	private registerEvents() {
		let _this = this;
		
		$('.alert').on('close.bs.alert', function () {
			$(this).hide();
			return false;
		});
		
		this.ratingStar.click(function (e) {
			e.preventDefault();
			_this.emptyStarErrorAlert.hide();
			_this.ratingStar.find('.fa').switchClass('fa-star', 'fa-star-o');
			
			let el: JQuery = $(this);
			let index: number = el.index();
			
			let numberSet = Array.apply(null, {length: index + 1}).map(Number.call, Number);
			numberSet.forEach(function (value) {
				_this.ratingStar.eq(value).find('.fa').switchClass('fa-star-o', 'fa-star');
			});
			
			_this.choosedStar = index + 1;
		});
		
		this.addItemForm.submit(function (e) {
			e.preventDefault();
			
			let titleValue: string = _this.itemTitle.val().toString();
			let hashtagsValue: string = _this.itemHashtags.val().toString();
			let textValue: string = _this.text.val().toString();
			
			if(!_this.uploadedImagesList.length) {
				_this.noImagesErrorAlert.show();
				return;
			}
			
			if(!_this.choosedStar) {
				_this.emptyStarErrorAlert.show();
				return;
			}

			AddItem.add(titleValue, hashtagsValue, _this.choosedStar, textValue, _this.uploadedImagesList, function (data) {
				runAction('contentItems', 'content', true, {'showAdded': '1', 'reviewId': data.reviewId});
			});
		});
		
		this.dropZone.on('drop', function (e: any) {
			e.preventDefault();
			e.stopPropagation();
			_this.startUpload(e.originalEvent.dataTransfer.files);
		});
		
		this.dropZone.on('dragover',function() {
			return false;
		});
		
		this.dropZone.on('dragleave',function() {
			return false;
		});
		
		this.uploadImagesFileInput.change(function (e: any) {
			_this.startUpload(e.originalEvent.target.files);
		});
	}
	
	private startUpload(files) {
		this.uploadedImagesList = [];
		let _this = this;
		let i, l = files.length;
		
		for(i = 0; i < l; i++) {
			if (!files[i].type.match(/image.*/)) {
				continue;
			}
			let reader = new FileReader();
			reader.onload = function (e: any) {
				_this.uploadedImagesList.push(e.target.result);
			};
			reader.readAsDataURL(files[i]);
		}
	}
	
	private translateText() {
		let _this = this;
		
		__('empty-star', 'content', function (e) {
			_this.emptyStarErrorAlert.find('.text').html(e);
		});
		__('no-images', 'content', function (e) {
			_this.noImagesErrorAlert.find('.text').html(e);
		});
	}
}

new addItemController().initialize();