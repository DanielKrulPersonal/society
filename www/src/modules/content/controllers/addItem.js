///  <reference path="../../../_includes.ts"/>
///  <reference path="../models/AddItem.ts"/>
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var addItemController = /** @class */ (function (_super) {
    __extends(addItemController, _super);
    function addItemController() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.choosedStar = null;
        _this.uploadedImagesList = [];
        return _this;
    }
    addItemController.prototype.initialize = function () {
        _super.prototype.registerController.call(this, 'AddItem', 'content');
        this.topPanelMainTemplateSpace = $('#js-topPanelMainTemplateSpace');
        this.topPanelHeight = this.topPanelMainTemplateSpace.parent().outerHeight();
        this.topPanelMainTemplateSpace.parent().hide();
        this.contextMenu = $('#js-contextMenu');
        this.addItemForm = $('#js-addItemForm');
        this.itemTitle = $('#js-title');
        this.itemHashtags = $('#js-hashtags');
        this.ratingStar = $('.js-rating-star');
        this.emptyStarErrorAlert = $('#js-emptyStar');
        this.text = $('#js-text');
        this.dropZone = $('#js-drop-zone');
        this.uploadImagesFileInput = $('#js-upload-images');
        this.noImagesErrorAlert = $('#js-no-images');
        this.contextMenu.height(this.topPanelHeight + 'px');
        this.ratingStar.find('.fa').switchClass('fa-star', 'fa-star-o');
        this.emptyStarErrorAlert.hide();
        this.noImagesErrorAlert.hide();
        this.registerEvents();
        this.translateText();
    };
    addItemController.prototype.registerEvents = function () {
        var _this = this;
        $('.alert').on('close.bs.alert', function () {
            $(this).hide();
            return false;
        });
        this.ratingStar.click(function (e) {
            e.preventDefault();
            _this.emptyStarErrorAlert.hide();
            _this.ratingStar.find('.fa').switchClass('fa-star', 'fa-star-o');
            var el = $(this);
            var index = el.index();
            var numberSet = Array.apply(null, { length: index + 1 }).map(Number.call, Number);
            numberSet.forEach(function (value) {
                _this.ratingStar.eq(value).find('.fa').switchClass('fa-star-o', 'fa-star');
            });
            _this.choosedStar = index + 1;
        });
        this.addItemForm.submit(function (e) {
            e.preventDefault();
            var titleValue = _this.itemTitle.val().toString();
            var hashtagsValue = _this.itemHashtags.val().toString();
            var textValue = _this.text.val().toString();
            if (!_this.uploadedImagesList.length) {
                _this.noImagesErrorAlert.show();
                return;
            }
            if (!_this.choosedStar) {
                _this.emptyStarErrorAlert.show();
                return;
            }
            AddItem.add(titleValue, hashtagsValue, _this.choosedStar, textValue, _this.uploadedImagesList, function (data) {
                runAction('contentItems', 'content', true, { 'showAdded': '1', 'reviewId': data.reviewId });
            });
        });
        this.dropZone.on('drop', function (e) {
            e.preventDefault();
            e.stopPropagation();
            _this.startUpload(e.originalEvent.dataTransfer.files);
        });
        this.dropZone.on('dragover', function () {
            return false;
        });
        this.dropZone.on('dragleave', function () {
            return false;
        });
        this.uploadImagesFileInput.change(function (e) {
            _this.startUpload(e.originalEvent.target.files);
        });
    };
    addItemController.prototype.startUpload = function (files) {
        this.uploadedImagesList = [];
        var _this = this;
        var i, l = files.length;
        for (i = 0; i < l; i++) {
            if (!files[i].type.match(/image.*/)) {
                continue;
            }
            var reader = new FileReader();
            reader.onload = function (e) {
                _this.uploadedImagesList.push(e.target.result);
            };
            reader.readAsDataURL(files[i]);
        }
    };
    addItemController.prototype.translateText = function () {
        var _this = this;
        __('empty-star', 'content', function (e) {
            _this.emptyStarErrorAlert.find('.text').html(e);
        });
        __('no-images', 'content', function (e) {
            _this.noImagesErrorAlert.find('.text').html(e);
        });
    };
    return addItemController;
}(Controller));
new addItemController().initialize();
