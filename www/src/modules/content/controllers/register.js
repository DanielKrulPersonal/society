///  <reference path="../../../_includes.ts"/>
var registerController = /** @class */ (function () {
    function registerController() {
    }
    registerController.prototype.initialize = function () {
        this.registerButton = $('#js-register-button');
        this.registerForm = $('#js-register-form');
        this.emailInput = $('#js-email-input');
        this.usernameInput = $('#js-username-input');
        this.passwordInput = $('#js-password-input');
        this.actionContentLoginToActionContentItems = $('#js-actionContentLogin-to-actionContentItems');
        this.usernameExistsErrorAlert = $('#js-usernameExistsError');
        this.emailExistsErrorAlert = $('#js-emailExistsError');
        this.registeredSuccessAlert = $('#js-registeredSuccessAlert');
        this.usernameExistsErrorAlert.hide();
        this.emailExistsErrorAlert.hide();
        this.registeredSuccessAlert.hide();
        this.registerEvents();
        this.translateText();
    };
    registerController.prototype.registerEvents = function () {
        var _this = this;
        this.registerForm.submit(function (e) {
            e.preventDefault();
            _this.registerButton.attr('disabled', 'disabled');
            var params = {
                'email': _this.emailInput.val(),
                'password': _this.passwordInput.val(),
                'username': _this.usernameInput.val()
            };
            ApiConnector.registerAction(params, function (data) {
                switch (data.statusCode) {
                    case ApiConnector.SUCCESS_REGISTERED_CODE: // register OK
                        _this.registeredSuccessAlert.show();
                        _this.emailInput.val('');
                        _this.usernameInput.val('');
                        _this.passwordInput.val('');
                        break;
                    case ApiConnector.ERROR_USERNAME_EXISTS_CODE: // username exists
                        _this.usernameExistsErrorAlert.show();
                        _this.usernameInput.val('');
                        break;
                    case ApiConnector.ERROR_EMAIL_EXISTS_CODE: // email exists
                        _this.emailExistsErrorAlert.show();
                        _this.emailInput.val('');
                        break;
                }
                _this.registerButton.removeAttr('disabled');
            });
        });
        $('.alert').on('close.bs.alert', function () {
            $(this).hide();
            return false;
        });
    };
    registerController.prototype.translateText = function () {
        var _this = this;
        __('placeholder-email', 'content', function (e) {
            _this.emailInput.attr('placeholder', e);
        });
        __('placeholder-password', 'content', function (e) {
            _this.passwordInput.attr('placeholder', e);
        });
        __('placeholder-username', 'content', function (e) {
            _this.usernameInput.attr('placeholder', e);
        });
        __('register-submit', 'content', function (e) {
            _this.registerButton.text(e);
        });
        __('username-exists', 'content', function (e) {
            _this.usernameExistsErrorAlert.find('.text').text(e);
        });
        __('email-exists', 'content', function (e) {
            _this.emailExistsErrorAlert.find('.text').text(e);
        });
        __('registered', 'content', function (e) {
            _this.registeredSuccessAlert.find('.text').html(e);
        });
    };
    return registerController;
}());
new registerController().initialize();
