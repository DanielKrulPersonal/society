///  <reference path="../../../_includes.ts"/>
var loginController = /** @class */ (function () {
    function loginController() {
    }
    loginController.prototype.initialize = function () {
        this.rememberLogin = $('#js-remember-login');
        this.loginButton = $('#js-login-button');
        this.loginForm = $('#js-login-form');
        this.emailInput = $('#js-email-input');
        this.passwordInput = $('#js-password-input');
        this.rememberLoginLabel = $('#js-label-remember-login');
        this.actionContentLoginToActionContentItems = $('#js-actionContentLogin-to-actionContentItems');
        this.wrongUsernameOrPasswordErrorAlert = $('#js-wrongUsernameOrPassword');
        this.wrongUsernameOrPasswordErrorAlert.hide();
        this.registerEvents();
        this.translateText();
    };
    loginController.prototype.registerEvents = function () {
        var _this = this;
        this.loginForm.submit(function (e) {
            e.preventDefault();
            var params = {
                'email': _this.emailInput.val(),
                'password': _this.passwordInput.val()
            };
            var rememberLogin = _this.rememberLogin.is(':checked');
            ApiConnector.loginAction(params, rememberLogin, function (data) {
                switch (data.statusCode) {
                    case ApiConnector.LOGIN_SUCCESS_LOGGED_CODE: // login OK
                        _this.actionContentLoginToActionContentItems.trigger('click');
                        break;
                    case ApiConnector.LOGIN_NOT_LOGGED_CODE: // wrong email or password
                        _this.wrongUsernameOrPasswordErrorAlert.show();
                        _this.emailInput.val('');
                        _this.passwordInput.val('');
                        break;
                }
            });
        });
        $('.alert').on('close.bs.alert', function () {
            $(this).hide();
            return false;
        });
    };
    loginController.prototype.translateText = function () {
        var _this = this;
        __('placeholder-email', 'content', function (e) {
            _this.emailInput.attr('placeholder', e);
        });
        __('placeholder-password', 'content', function (e) {
            _this.passwordInput.attr('placeholder', e);
        });
        __('label-remember-login', 'content', function (e) {
            _this.rememberLoginLabel.text(e);
        });
        __('login-submit', 'content', function (e) {
            _this.loginButton.text(e);
        });
        __('wrong-email-or-password', 'content', function (e) {
            _this.wrongUsernameOrPasswordErrorAlert.find('.text').html(e);
        });
    };
    return loginController;
}());
new loginController().initialize();
