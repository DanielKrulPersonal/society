///  <reference path="../../_includes.ts"/>
class contentModule extends Module {
	private contentMainTemplate: Template;
	static contentItemsAction;

	public initialize() {
		super.initialize();
		super.newTemplate('items', 'w-100');
		super.newTemplate('login', 'w-100');
		super.newTemplate('settings');
		super.newTemplate('user');
		super.newTemplate('addItem', 'w-100');
		super.newTemplate('register', 'w-100');

		this.contentMainTemplate = super.newTemplate('content', 'h-100 d-flex order-1');

		template('system', 'system').add(this.contentMainTemplate);

		this.setAction('contentItems', this.actionContentItems);
		this.setAction('contentSettings', this.actionContentSettings);
		this.setAction('contentLogin', this.actionContentLogin);
		this.setAction('contentUser', this.actionContentUser);
		this.setAction('contentAddItem', this.actionContentAddItem);
		this.setAction('contentRegister', this.actionContentRegister);
		this.setAction('logout', this.actionLogout);

		contentModule.contentItemsAction = action('contentItems', 'content');
		runAction('contentItems', 'content', false);
	}

	public deleteTemplates() {
		template('items', 'content').deleteHtml();
		template('login', 'content').deleteHtml();
		template('settings', 'content').deleteHtml();
		template('user', 'content').deleteHtml();
		template('addItem', 'content').deleteHtml();
		template('register', 'content').deleteHtml();
	}

	public actionContentItems() {
		let _this: contentModule = contentModule.contentItemsAction['caller'];
		_this.deleteTemplates();

		let t: Template = template('items', 'content');
		t.addController();

		_this.contentMainTemplate.add(t);
	}

	public actionContentSettings() {
		let _this: contentModule = contentModule.contentItemsAction['caller'];
		_this.deleteTemplates();

		_this.contentMainTemplate.add(template('settings', 'content'));
	}

	public actionContentLogin() {
		let _this: contentModule = contentModule.contentItemsAction['caller'];
		_this.deleteTemplates();

		let t: Template = template('login', 'content');
		t.addController();

		_this.contentMainTemplate.add(t);
	}

	public actionContentUser() {
		let _this: contentModule = contentModule.contentItemsAction['caller'];
		_this.deleteTemplates();

		let t: Template = template('user', 'content');
		//t.addController();

		_this.contentMainTemplate.add(t);
	}

	public actionContentAddItem() {
		let _this: contentModule = contentModule.contentItemsAction['caller'];
		_this.deleteTemplates();

		let t: Template = template('addItem', 'content');
		t.addController();


		_this.contentMainTemplate.add(t);
	}

	public actionContentRegister() {
		let _this: contentModule = contentModule.contentItemsAction['caller'];
		_this.deleteTemplates();

		let t: Template = template('register', 'content');
		t.addController();


		_this.contentMainTemplate.add(t);
	}

	public actionLogout() {
		localStorage.removeItem('login_data');
		localStorage.removeItem('token');
		sessionStorage.removeItem('login_data');
		sessionStorage.removeItem('token');

		runAction('contentItems', 'content');
	}
}

function contentModuleInstancer() {
	return new contentModule();
}