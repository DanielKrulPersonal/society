///  <reference path="../../../_includes.ts"/>
var AddItem = /** @class */ (function () {
    function AddItem() {
    }
    AddItem.add = function (title, hashtags, stars, text, uploadedImages, callback) {
        var token = localStorage.getItem('token') || sessionStorage.getItem('token');
        var loginData = localStorage.getItem('login_data') || sessionStorage.getItem('login_data');
        var email = null;
        if (token && loginData) {
            var loginDataParsed = JSON.parse(CryptoJS.AES.decrypt(loginData, token + App.APP_SALT).toString(CryptoJS.enc.Utf8));
            email = loginDataParsed.email;
        }
        ApiConnector.addItem({
            'title': title,
            'hashtags': hashtags,
            'lang': App.config['lang'],
            'user': email,
            'stars': stars,
            'text': text,
            'images': JSON.stringify(uploadedImages)
        }, function (response) {
            callback(response);
        });
    };
    return AddItem;
}());
