///  <reference path="../../../_includes.ts"/>
class ReviewsList {
	static getList(limit: number, offset: number, callback) {
		ApiConnector.getList({'limit': limit, 'offset': offset, 'lang': App.config['lang']},function(response) {
			callback(response);
		});
	}
}