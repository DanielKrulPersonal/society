///  <reference path="../../../_includes.ts"/>
var ReviewsList = /** @class */ (function () {
    function ReviewsList() {
    }
    ReviewsList.getList = function (limit, offset, callback) {
        ApiConnector.getList({ 'limit': limit, 'offset': offset, 'lang': App.config['lang'] }, function (response) {
            callback(response);
        });
    };
    return ReviewsList;
}());
