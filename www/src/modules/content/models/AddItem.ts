///  <reference path="../../../_includes.ts"/>
class AddItem {
	static add (title: string, hashtags: string, stars: number, text: string, uploadedImages: string[], callback) {
		let token = localStorage.getItem('token') || sessionStorage.getItem('token');
		let loginData = localStorage.getItem('login_data') || sessionStorage.getItem('login_data');
		let email: string = null;
		
		if(token && loginData) {
			let loginDataParsed = JSON.parse(CryptoJS.AES.decrypt(loginData, token + App.APP_SALT).toString(CryptoJS.enc.Utf8));
			email = loginDataParsed.email;
		}
		
		ApiConnector.addItem({
			'title': title,
			'hashtags': hashtags,
			'lang': App.config['lang'],
			'user': email,
			'stars': stars,
			'text': text,
			'images': JSON.stringify(uploadedImages)
		},function(response) {
			callback(response);
		});
	}
}