var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///  <reference path="../../_includes.ts"/>
var contentModule = /** @class */ (function (_super) {
    __extends(contentModule, _super);
    function contentModule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    contentModule.prototype.initialize = function () {
        _super.prototype.initialize.call(this);
        _super.prototype.newTemplate.call(this, 'items', 'w-100');
        _super.prototype.newTemplate.call(this, 'login', 'w-100');
        _super.prototype.newTemplate.call(this, 'settings');
        _super.prototype.newTemplate.call(this, 'user');
        _super.prototype.newTemplate.call(this, 'addItem', 'w-100');
        _super.prototype.newTemplate.call(this, 'register', 'w-100');
        this.contentMainTemplate = _super.prototype.newTemplate.call(this, 'content', 'h-100 d-flex order-1');
        template('system', 'system').add(this.contentMainTemplate);
        this.setAction('contentItems', this.actionContentItems);
        this.setAction('contentSettings', this.actionContentSettings);
        this.setAction('contentLogin', this.actionContentLogin);
        this.setAction('contentUser', this.actionContentUser);
        this.setAction('contentAddItem', this.actionContentAddItem);
        this.setAction('contentRegister', this.actionContentRegister);
        this.setAction('logout', this.actionLogout);
        contentModule.contentItemsAction = action('contentItems', 'content');
        runAction('contentItems', 'content', false);
    };
    contentModule.prototype.deleteTemplates = function () {
        template('items', 'content').deleteHtml();
        template('login', 'content').deleteHtml();
        template('settings', 'content').deleteHtml();
        template('user', 'content').deleteHtml();
        template('addItem', 'content').deleteHtml();
        template('register', 'content').deleteHtml();
    };
    contentModule.prototype.actionContentItems = function () {
        var _this = contentModule.contentItemsAction['caller'];
        _this.deleteTemplates();
        var t = template('items', 'content');
        t.addController();
        _this.contentMainTemplate.add(t);
    };
    contentModule.prototype.actionContentSettings = function () {
        var _this = contentModule.contentItemsAction['caller'];
        _this.deleteTemplates();
        _this.contentMainTemplate.add(template('settings', 'content'));
    };
    contentModule.prototype.actionContentLogin = function () {
        var _this = contentModule.contentItemsAction['caller'];
        _this.deleteTemplates();
        var t = template('login', 'content');
        t.addController();
        _this.contentMainTemplate.add(t);
    };
    contentModule.prototype.actionContentUser = function () {
        var _this = contentModule.contentItemsAction['caller'];
        _this.deleteTemplates();
        var t = template('user', 'content');
        //t.addController();
        _this.contentMainTemplate.add(t);
    };
    contentModule.prototype.actionContentAddItem = function () {
        var _this = contentModule.contentItemsAction['caller'];
        _this.deleteTemplates();
        var t = template('addItem', 'content');
        t.addController();
        _this.contentMainTemplate.add(t);
    };
    contentModule.prototype.actionContentRegister = function () {
        var _this = contentModule.contentItemsAction['caller'];
        _this.deleteTemplates();
        var t = template('register', 'content');
        t.addController();
        _this.contentMainTemplate.add(t);
    };
    contentModule.prototype.actionLogout = function () {
        localStorage.removeItem('login_data');
        localStorage.removeItem('token');
        sessionStorage.removeItem('login_data');
        sessionStorage.removeItem('token');
        runAction('contentItems', 'content');
    };
    return contentModule;
}(Module));
function contentModuleInstancer() {
    return new contentModule();
}
