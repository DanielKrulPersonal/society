var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
///  <reference path="../../_includes.ts"/>
var panelModule = /** @class */ (function (_super) {
    __extends(panelModule, _super);
    function panelModule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    panelModule.prototype.initialize = function () {
        _super.prototype.initialize.call(this);
        var t;
        t = _super.prototype.newTemplate.call(this, 'hamburger', 'col-2 hamburger text-center');
        t.addController();
        t = _super.prototype.newTemplate.call(this, 'searchBox', 'col-11');
        t.addController();
        _super.prototype.newTemplate.call(this, 'rightActions', 'col-1');
        var panelTemplate = _super.prototype.newTemplate.call(this, 'panel', 'order-0');
        panelTemplate.addController();
        template('system', 'system').add(panelTemplate);
    };
    return panelModule;
}(Module));
function panelModuleInstancer() {
    return new panelModule();
}
