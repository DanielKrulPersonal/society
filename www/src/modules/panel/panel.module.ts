///  <reference path="../../_includes.ts"/>
class panelModule extends Module {
	public initialize() {
		super.initialize();

		let t: Template;

		t = super.newTemplate('hamburger', 'col-2 hamburger text-center');
		t.addController();
		t = super.newTemplate('searchBox', 'col-11');
		t.addController();
		super.newTemplate('rightActions', 'col-1');

		let panelTemplate: Template = super.newTemplate('panel', 'order-0');
		panelTemplate.addController();

		template('system', 'system').add(panelTemplate);
	}
}

function panelModuleInstancer() {
	return new panelModule();
}