///  <reference path="../../../_includes.ts"/>
var panelController = /** @class */ (function () {
    function panelController() {
    }
    panelController.prototype.initialize = function () {
        this.templateSpace = $('#js-topPanelMainTemplateSpace');
        this.hamburgerTemplate = template('hamburger', 'panel');
        this.searchBoxTemplate = template('searchBox', 'panel');
        this.rightActionsTemplate = template('rightActions', 'panel');
        this.processInnerTemplates();
    };
    panelController.prototype.processInnerTemplates = function () {
        var _this = this;
        this.hamburgerTemplate.render(function () {
            _this.searchBoxTemplate.render(function (e) {
                _this.searchBoxTemplate.add(_this.hamburgerTemplate);
                _this.templateSpace.append(e);
                _this.rightActionsTemplate.render(function (e) {
                    _this.templateSpace.append(e);
                });
            });
        });
    };
    return panelController;
}());
new panelController().initialize();
