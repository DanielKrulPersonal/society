///  <reference path="../../../_includes.ts"/>
class panelController {
	private templateSpace: JQuery;
	private hamburgerTemplate: Template;
	private searchBoxTemplate: Template;
	private rightActionsTemplate: Template;

	public initialize() {
		this.templateSpace = $('#js-topPanelMainTemplateSpace');

		this.hamburgerTemplate = template('hamburger', 'panel');
		this.searchBoxTemplate = template('searchBox', 'panel');
		this.rightActionsTemplate = template('rightActions', 'panel');

		this.processInnerTemplates();
	}

	private processInnerTemplates() {
		let _this = this;

		this.hamburgerTemplate.render(function() {
			_this.searchBoxTemplate.render(function(e) {
				_this.searchBoxTemplate.add(_this.hamburgerTemplate);
				_this.templateSpace.append(e);

				_this.rightActionsTemplate.render(function(e) {
					_this.templateSpace.append(e);
				});
			});
		});
	}
}

new panelController().initialize();