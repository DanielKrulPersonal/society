///  <reference path="../../../_includes.ts"/>
var hamburgerController = /** @class */ (function () {
    function hamburgerController() {
    }
    hamburgerController.prototype.initialize = function () {
        this.buttonToggleSideMenu = $('#js-toggleSideMenu');
        this.mainToggleMenu = $('#js-mainToggleMenu');
        this.mainToggleMenuShadow = $('#js-mainToggleMenuShadow');
        this.screenWidth = $('body').width();
        this.sideMenuHrefs = $('#js-sideMenuHrefs');
        this.sideMenuHomeHref = $('#js-sideMenuHomeHref');
        this.sideMenuSettingsHref = $('#js-sideMenuSettingsHref');
        this.sideMenuAboutHref = $('#js-sideMenuAboutHref');
        this.sideMenuLoginOrProfileHref = $('#js-sideMenuLoginOrProfileHref');
        this.clickToCloseSideMenuHref = $('.js-clickToCloseSideMenu');
        this.registerEvents();
        this.translateText();
    };
    hamburgerController.prototype.translateText = function () {
        var _this = this;
        __('side-menu-home-href', 'panel', function (e) {
            _this.sideMenuHomeHref.html(e);
        });
        __('side-menu-settings-href', 'panel', function (e) {
            _this.sideMenuSettingsHref.html(e);
        });
        __('side-menu-about-href', 'panel', function (e) {
            _this.sideMenuAboutHref.html(e);
        });
        __('side-menu-loginOrProfile-href', 'panel', function (e) {
            _this.sideMenuLoginOrProfileHref.html(e);
        });
    };
    hamburgerController.prototype.closeSideMenu = function () {
        this.mainToggleMenuShadow.hide();
        this.mainToggleMenu.animate({ width: '0' }, 350);
        this.sideMenuState = false;
    };
    hamburgerController.prototype.openSideMenu = function () {
        var optimalSideMenuSize = 0;
        if (this.screenWidth <= 360) {
            optimalSideMenuSize = 240;
        }
        else if (this.screenWidth <= 600) {
            optimalSideMenuSize = 300;
        }
        else if (this.screenWidth <= 768) {
            optimalSideMenuSize = 470;
        }
        else {
            optimalSideMenuSize = 600;
        }
        this.sideMenuHrefs.css('width', optimalSideMenuSize + 'px');
        this.mainToggleMenuShadow.show();
        this.mainToggleMenu.animate({ width: optimalSideMenuSize + 'px' }, 400);
        this.sideMenuState = true;
    };
    hamburgerController.prototype.openCloseSideMenuEvent = function () {
        /*
        true = otevřeno
        false = zavřeno
         */
        if (this.sideMenuState) {
            this.closeSideMenu();
            return;
        }
        this.openSideMenu();
    };
    hamburgerController.prototype.registerEvents = function () {
        var _this = this;
        this.clickToCloseSideMenuHref.click(function () {
            _this.closeSideMenu();
        });
        this.buttonToggleSideMenu.click(function () {
            _this.openCloseSideMenuEvent();
        });
        this.mainToggleMenuShadow.click(function () {
            _this.closeSideMenu();
        });
        //let lastLeftOrRightX = 0, lastDirection = '';
        function touchStart() {
            $('body').on('touchmove', function (e) {
                var touchMoveObject = e.changedTouches[0];
                _this.mainToggleMenu.animate({ width: touchMoveObject.screenX + 'px' }, 4);
                /*if(lastLeftOrRightX < touchMoveObject.screenX) {
                    if(lastDirection !== 'right') {
                        _this.openSideMenu();
                    }

                    lastDirection = 'right';
                } else {
                    if(lastDirection !== 'left') {
                        _this.closeSideMenu();
                    }

                    lastDirection = 'left';
                }
                lastLeftOrRightX = touchMoveObject.screenX;*/
            });
            $('body').on('touchend', function () {
                $('body').unbind('touchstart', touchStart);
            });
        }
        //$('body').on('touchstart', touchStart);
    };
    return hamburgerController;
}());
new hamburgerController().initialize();
