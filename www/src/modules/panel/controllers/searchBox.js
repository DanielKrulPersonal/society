///  <reference path="../../../_includes.ts"/>
var searchBoxController = /** @class */ (function () {
    function searchBoxController() {
    }
    searchBoxController.prototype.initialize = function () {
        this.mainSearchInput = $('#js-main-search-input');
        this.translateText();
        this.registerEvents();
    };
    searchBoxController.prototype.registerEvents = function () {
        this.mainSearchInput.hashTagInput({
            showHashTags: true,
            hideInput: true,
            formatAsBubbles: true,
            wrapTags: true
        });
    };
    searchBoxController.prototype.translateText = function () {
        var _this = this;
        __('main-search-input-placeholder', 'panel', function (e) {
            _this.mainSearchInput.attr('placeholder', e);
        });
        /*__('add-item-button-title', 'topPanel', function (e) {
            _this.butttonAddItem.attr('title', e);
        });*/
    };
    return searchBoxController;
}());
new searchBoxController().initialize();
