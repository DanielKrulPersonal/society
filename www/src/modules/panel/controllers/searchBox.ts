///  <reference path="../../../_includes.ts"/>

interface JQuery {
	hashTagInput(options?: any) : any;
}

class searchBoxController {
	private mainSearchInput: JQuery;

	public initialize() {
		this.mainSearchInput = $('#js-main-search-input');

		this.translateText();
		this.registerEvents();
	}
	
	private registerEvents() {
		this.mainSearchInput.hashTagInput({
			showHashTags: true,
			hideInput: true,
			formatAsBubbles: true,
			wrapTags: true
		});
	}

	private translateText() {
		let _this = this;
		__('main-search-input-placeholder', 'panel', function (e) {
			_this.mainSearchInput.attr('placeholder', e);
		});

		/*__('add-item-button-title', 'topPanel', function (e) {
			_this.butttonAddItem.attr('title', e);
		});*/
	}
}

new searchBoxController().initialize();