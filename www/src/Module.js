///  <reference path="_includes.ts"/>
var Module = /** @class */ (function () {
    function Module() {
    }
    Module.prototype.initialize = function () {
        this.pathModuleName = this.parentModuleName ? this.parentModuleName : this.moduleName;
        this.templatesPath = App.ROOT_DIR + 'modules/' + this.pathModuleName + '/templates';
    };
    /**
     * @param {string} templateName
     * @param {string} classNames
     * @returns {Template}
     */
    Module.prototype.newTemplate = function (templateName, classNames) {
        if (classNames === void 0) { classNames = ''; }
        return new Template(templateName, this.templatesPath + '/' + templateName + '.html', classNames, this.pathModuleName);
    };
    /**
     * @param {string} actionName
     * @param method
     */
    Module.prototype.setAction = function (actionName, method) {
        Module.actionsInstances[actionName + '__' + this.moduleName] = { 'method': method, 'caller': this };
    };
    Module.actionsInstances = {};
    Module.lastCalledAction = {};
    return Module;
}());
/**
 * @param {string} actionName
 * @param {string} moduleName
 * @returns {object}
 */
function action(actionName, moduleName) {
    return Module.actionsInstances[actionName + '__' + moduleName];
}
/**
 *
 * @param {string} actionName
 * @param {string} moduleName
 * @param {boolean} inUrl
 * @param {object} payload
 */
function runAction(actionName, moduleName, inUrl, payload) {
    if (inUrl === void 0) { inUrl = true; }
    if (payload === void 0) { payload = {}; }
    var emptyFunc = function () { };
    if (Module.lastCalledAction['moduleName'] === moduleName && Module.lastCalledAction['moduleAction'] === actionName)
        return { 'method': emptyFunc };
    Module.lastCalledAction = { 'moduleName': moduleName, 'moduleAction': actionName };
    if (inUrl) {
        var urlData = {
            'action': actionName,
            'module': moduleName
        };
        $.extend(urlData, payload);
        history.pushState({}, null, '?' + $.param(urlData));
    }
    action(actionName, moduleName)['method']();
}
