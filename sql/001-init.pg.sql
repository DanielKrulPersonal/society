CREATE EXTENSION pg_trgm;
CREATE EXTENSION btree_gist;

CREATE TABLE public.users
(
  email character varying(100) NOT NULL,
  username character varying(100) NOT NULL,
  password character varying(64) NOT NULL,
  token character varying(64) NOT NULL,
  CONSTRAINT users_email PRIMARY KEY (email)
)
WITH (
  OIDS=FALSE
);

CREATE INDEX users_email_password
  ON public.users
  USING btree
  (email COLLATE pg_catalog."default", password COLLATE pg_catalog."default");

CREATE SEQUENCE reviews_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."reviews" (
    "id" integer DEFAULT nextval('reviews_id_seq') NOT NULL,
    "user" character varying(100),
    "title" character varying(255) NOT NULL,
    "lang" character varying(10) NOT NULL,
    "stars" smallint NOT NULL,
    "enabled" boolean NOT NULL,
    "hashtags" text,
    "image" character varying(255),
    "barcodes" text,
    CONSTRAINT "reviews_id" PRIMARY KEY ("id")
) WITH (oids = false);