-- Adminer 4.6.2 PostgreSQL dump

DROP TABLE IF EXISTS "reviews";
DROP SEQUENCE IF EXISTS reviews_id_seq;
CREATE SEQUENCE reviews_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."reviews" (
    "id" integer DEFAULT nextval('reviews_id_seq') NOT NULL,
    "user" character varying(100),
    "title" character varying(255) NOT NULL,
    "lang" character varying(10) NOT NULL,
    "enabled" boolean NOT NULL,
    "hashtags" text,
    "image" character varying(255),
    "barcodes" text,
    "sponsored" boolean,
    "created_at" timestamp,
    CONSTRAINT "reviews_id" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "reviews_enabled_lang" ON "public"."reviews" USING btree ("enabled", "lang");

TRUNCATE "reviews";
INSERT INTO "reviews" ("id", "user", "title", "lang", "enabled", "hashtags", "image", "barcodes", "sponsored", "created_at") VALUES
(1,	'daniel.krul@poski.com',	'Testovací položka',	'cs-CZ',	't',	'#jaffa#kree#Hvězdná brána#Letadlo',	'203452-top_foto1-ho1fq.jpg',	NULL,	NULL,	NULL),
(5,	'daniel.krul@poski.com',	'Blackberry Keyone',	'cs-CZ',	't',	NULL,	'104428686-BlackBerry-Keyone-11.JPG?v=1493227132',	NULL,	NULL,	NULL),
(2,	'daniel.krul@poski.com',	'Gnome 3',	'cs-CZ',	't',	NULL,	'korora-gnome-desktop-apps.jpg',	NULL,	NULL,	NULL),
(3,	'daniel.krul@poski.com',	'Cinestar Ostrava',	'cs-CZ',	't',	NULL,	'cinestar-ostrava-1.jpg',	NULL,	NULL,	NULL),
(6,	'daniel.krul@poski.com',	'Macbook Pro',	'cs-CZ',	't',	NULL,	'apple-macbook-pro-with-touch-bar-13-inch-2016-17.jpg',	NULL,	't',	NULL),
(4,	'daniel.krul@poski.com',	'Hurghada',	'cs-CZ',	't',	NULL,	'hurghada-Mahmya-Island-1112x630-678x381.jpg',	NULL,	't',	NULL);

DROP TABLE IF EXISTS "reviews_items";
DROP SEQUENCE IF EXISTS reviews_items_id_seq;
CREATE SEQUENCE reviews_items_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."reviews_items" (
    "id" integer DEFAULT nextval('reviews_items_id_seq') NOT NULL,
    "reviews_id" integer NOT NULL,
    "stars" smallint NOT NULL,
    "text" text,
    CONSTRAINT "reviews_items_id" PRIMARY KEY ("id")
) WITH (oids = false);

TRUNCATE "reviews_items";
INSERT INTO "reviews_items" ("id", "reviews_id", "stars", "text") VALUES
(1,	5,	5,	'Blackberry vydalo konečně super telefon. Hlavně je odolný a sexy'),
(2,	5,	2,	'Telefon je strašně nevýkonný a upadl mu display'),
(3,	5,	4,	'Nic  zajímavého, jsem zvyklý na klasickou kvalitu'),
(4,	4,	3,	'Podle vyprávění je Hurghada super'),
(5,	6,	4,	'Super, ale strašně drahé'),
(6,	4,	4,	'TEST'),
(7,	4,	1,	'Další nesmysl'),
(8,	4,	4,	'Jasdfsf'),
(9,	4,	3,	'asdsadad'),
(10,	4,	5,	'xcvb'),
(11,	4,	4,	'dfhedhdfh'),
(12,	4,	3,	'yxvvvv'),
(13,	4,	3,	'yxvvvv'),
(14,	4,	3,	'yxvvvv'),
(15,	4,	3,	'yxvvvv'),
(18,	4,	3,	'yxvvvv'),
(20,	4,	3,	'yxvvvv'),
(21,	4,	3,	'yxvvvv'),
(22,	4,	3,	'yxvvvv'),
(23,	4,	3,	'yxvvvv'),
(24,	4,	3,	'yxvvvv'),
(25,	4,	3,	'yxvvvv'),
(26,	4,	3,	'yxvvvv'),
(27,	4,	3,	'yxvvvv'),
(28,	4,	3,	'yxvvvv'),
(29,	4,	3,	'yxvvvv'),
(30,	4,	3,	'yxvvvv'),
(31,	4,	3,	'yxvvvv'),
(32,	4,	3,	'yxvvvv'),
(33,	4,	3,	'yxvvvv'),
(34,	4,	3,	'yxvvvv'),
(35,	4,	3,	'yxvvvv'),
(36,	4,	4,	'asff'),
(37,	4,	4,	'sdgf'),
(40,	4,	4,	NULL);

DROP TABLE IF EXISTS "users";
CREATE TABLE "public"."users" (
    "email" character varying(100) NOT NULL,
    "username" character varying(100) NOT NULL,
    "password" character varying(64) NOT NULL,
    "token" character varying(64) NOT NULL,
    CONSTRAINT "users_email" PRIMARY KEY ("email")
) WITH (oids = false);

CREATE INDEX "users_email_password" ON "public"."users" USING btree ("email", "password");

TRUNCATE "users";
INSERT INTO "users" ("email", "username", "password", "token") VALUES
('daniel.krul@poski.com',	'Krulik',	'03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4',	'93d599c0e0ea294c14019e3f8848810d'),
('pkrul@seznam.cz',	'Krulik25',	'03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4',	'52ea5cab53aadd4b4e6635ecb48887ec'),
('jaffa@poski.com',	'fsdfsdfds',	'03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4',	'18c1385247f52e2ad8856c40ccbc5e33'),
('sdfa@ertger.cz',	'1234',	'b273e15c07bf99b5139e5753946e004d663e83b3eadb4c8dea699ee982573ef0',	'262a90f87af6aab77edb0617b0c27e65'),
('jaffa@jakka.com',	'sijfisdojfio',	'9cf4df85346c882c68327787955fd530f793f93d2058a5c868f2d6a003e27998',	'325a3d7b52d5aa44721b13b054246d08'),
('jaffa@dsfgg.com',	'sdfsdcsdfs',	'03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4',	'6cd909921e2c83c159fa59bad49beaed'),
('asd@fhfg.cz',	'ghjghj',	'8137c3f76d3714ae5ad3bb59994562f4bbeb1155c09c0e0add825a61d73c8c02',	'b51917805f31d3ba42a276793c966337');

-- 2018-06-06 17:41:32.094085+02
